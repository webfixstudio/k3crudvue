<?php

use Faker\Generator as Faker;

// factory(App\Woker::class, 5)->make();
// factory(App\Woker::class, 5)->create();

$factory->define(App\Woker::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'birthday' => $faker->dateTime(),
        'avatar' => 'https://placeimg.com/250/250/people',
        'email' => $faker->unique()->safeEmail,
    ];
});
